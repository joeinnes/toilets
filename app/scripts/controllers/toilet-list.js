'use strict';

/**
 * @ngdoc function
 * @name toiletsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the toiletsApp
 */
angular.module('toiletsApp')

.controller('ListToiletsCtrl', ['$scope', 'toiletList', function($scope, toiletList){
    $scope.toilets = toiletList;
}])
