'use strict';

/**
 * @ngdoc function
 * @name toiletsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the toiletsApp
 */
angular.module('toiletsApp')

.controller('NearestCtrl', ['$scope', 'toiletList', function($scope, toiletList){
    // TODO: Move this into a service
    if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position){
                $scope.$apply(function(){
                    $scope.position = position;
                });
            });
        }


    // FIXME: Static datasource for testing
    var closestToilet;
    $scope.closestToilet = toiletList[0];
}])
