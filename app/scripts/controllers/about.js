'use strict';

/**
 * @ngdoc function
 * @name toiletsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the toiletsApp
 */
angular.module('toiletsApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
