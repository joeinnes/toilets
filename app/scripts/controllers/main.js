'use strict';

/**
 * @ngdoc function
 * @name toiletsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the toiletsApp
 */
angular.module('toiletsApp')
.controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
        'HTML5 Boilerplate',
        'AngularJS',
        'Karma'
    ];
});

