'use strict';

/**
 * @ngdoc overview
 * @name toiletsApp
 * @description
 * # toiletsApp
 *
 * Main module of the application.
 */
angular.module('toiletsApp', [
    'ngAnimate',
    'ngCookies',
    'ngMap',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
])
// FUTURE: Modify this value declaration to return a JSON fetched from an API
    .value('toiletList', [
    {
        "name": "Home toilet",
        "description": "This is my toilet at home. It is comfortable and convenient. It has a few problems with the flush mechanism though.",
        "rating": 5,
        "reviews": [
            {
                "title": "Top notch",
                "body": "Great toilet, would use again +++",
                "author": "Joe Innes"
            },
            {
            "title": "OK",
            "body": "Decent enough. Wouldn't want to use this every day though.",
            "author": "John Doe"
            }
        ],
        "location": {
               "latitude": 47.5105714,
               "longitude": 19.052812199999998
               }
    },
     {
               "name": "Home toilet 2",
               "description":
               " 2 This is my toilet at home. It is comfortable and convenient. It has a few problems with the flush mechanism though.",
               "rating": 3,
               "reviews": "2 Great toilet, would use again +++",
               "location": {
               "latitude": 47.5105714,
               "longitude": 19.052812199999998
               }
               }])


.config(function ($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
    })
    .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
    })
    .when('/nearest', {
        templateUrl: 'views/nearest.html',
        controller: 'NearestCtrl'
    })
    .when('/toilet-list', {
        templateUrl: 'views/toilet-list.html',
        controller: 'ListToiletsCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
});

